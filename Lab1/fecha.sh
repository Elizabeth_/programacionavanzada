#!/bin/bash

fecha=`date +%Y%m%d`
anio=`date +%Y`
mes=`date +%m`
dia=`date +%d`

if [[ $2 ]]; then
  echo "Error, no se puede ingresar más de un parametro"
  exit 1
fi

if [[ $1 == "-s" || $1 == "--short" ]]; then
  echo "$dia/$mes/$anio"
  exit 1
elif [[ $1 == "-l" || $1 == "--long" ]]; then
  echo " Dia: $dia , mes $mes del año $anio"
  exit 1
elif [[ $# == 0 ]]; then
  cal
  exit 1

elif [[ $1 -ne "-s" || $1 -ne "--short" || $1 -ne "-l" || $1 -ne "--long" ]]; then
  echo "Incorrecto !!!"
fi
