#!/bin/bash

#ingresar ID de proteína
function ImagenPNG {
		
		
	echo "Ingrese identificador de la proteina: "
			read proteinName 
	proteinName=${proteinName^^} 
	variable=$(grep -w $proteinName bd-pdb.txt | awk -F '",' '{print $4}') 
	#~ echo $variable

	if [[ $variable == '"Protein"' ]]; then
		wget https://files.rcsb.org/download/$proteinName.pdb
		awk '{if ($1=="ATOM") {print substr($0,18,3), substr($0,24,4), substr ($0,14,3), substr($0,22,1)}}' $proteinName.pdb > ATOM_$proteinName.txt
		echo 'digraph G {' >> amino_$proteinName.dot
		echo 'rankdir=LR;' >> amino_$proteinName.dot 
		echo 'node [shape=box]' >> amino_$proteinName.dot
		awk '{print $1$2$4, $4}' ATOM_$proteinName.txt >> amino_search_$proteinName.txt
		awk '{print $3$2$1$4 "[ label = \""$3"\" ]"
			  print $3$2$1$4 " ""->"" " $1$2$4}' ATOM_$proteinName.txt >> amino_$proteinName.dot
		awk '{print $1$2$4 " ""[ label = \""$1"\" ]"}' ATOM_$proteinName.txt >> amino_$proteinName.dot
		awk '($1!=p && k==$2 ){print p " ""->"" "$1 "[ label = \""$2"\" ];"; s=0} {p=$1; s+=$2; k=$2} {n = $1} END{}' amino_search_$proteinName.txt >> amino_$proteinName.dot
		
		echo -e "}" >> amino_$proteinName.dot
		
		#El siguiente comando nos permite visualizar el grafo de forma rápida, 
		# así no hay problemas con el tiempo que demora en cargar la imagen.png
		xdot amino_$proteinName.dot
		
		#Con este comando formamos una imagen.png
		#~ dot -Tsvg -o "$proteinName".svg amino_"$proteinName".dot
	else
		echo "no es proteina, vuelva a correr el programa"
		exit 1
	fi

}

opcion=0
while [[ $opcion != 3 ]]; do
	
	echo "  ---------------------MENÚ-----------------------"
	echo " 	  -------------------------------"
	echo "     ..:::::::: Análisis de proteínas :::::::.."
	echo "           -------------------------------"

	echo "	"
	echo "  (1) Leer descripción del Proyecto "
	echo "  (2) Generar Imagen "
	echo "  (3) Salir "



	echo "Ingrese la opción deseada "
	read opcion
		
	if [[ $opcion == 1 ]]; then 
		cat README.txt
		
	elif [[ $opcion == 2 ]]; then 
		ImagenPNG 
		exit 1
		
	elif [[ $opcion == 3 ]]; then 
		echo "Vuelva pronto "
		exit 1
		
	else 
		echo "Opción no válida, Intente nuevamente"
		
	fi

done


    



