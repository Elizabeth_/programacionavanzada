Descripción:

El propósito de este programa, es permitir al usuario visualizar de forma clara y organizada lo aminoacidos (y sus atomos) de archivos en formato *.pdb. En este tipo de archivos, se encuentra mucha información que es utilizada por otros programas o simplemente son detalles adicionales que, en general, puede dificultar el estudio de la proteína a nivel más básico. Este programa lee un archivo y muestra sólo los aminoácidos y sus respectivos átomos, mediante un grafo generado.

Requerimientos:

-XDot para visualizar la imagen apenas sea generada. Sin embargo, el archivo de imagen quedará guardado y podrá ser visualizado en otro momento con cualquier visor de imagene que soporte formato *.dot y en una imagen *.png


Modo de uso:

1) Compilar el programa: En la terminal, diríjase a la carpeta del programa con el comando "cd", luego, al estar en la carpeta del proyecto, se coloca bash Nombre_Del_proyecto.sh, donde se nos mostrará un menú, el cual nos dará las opciones a) ver descripción del proyecto b)crear una imagen de una proteina a gusto. c) salir del programa. 

Advertencias: 
	-La imagen se guardará en un archivo con el nombre  de la proteina, la imagen se generaá automáticamente.sin embargo al momento de mostrar el grafo, lo haremos mediante "Xdot", ya que al crear la imagen esta se demora bastante, causando que el computador se peque. 
La linea de codigo que nos permite crear una imagen.png está comentada por este motivo, si se descomenta se crea la imagen luego de un tiempo. 
 
 	Si un archivo con el mismo nombre se encuentra en la carpeta (de alguna visualización anterior), será sobreescrito. 

El Poyecto busca poder generar de manera fácil, cómoda y utilizando información trabajada en clases una imagen.png, donde se podrán visualizar los aminoácidos que componen esta proteina y sus elementos.
 De acuerdo a este objetivo, el trabajo que se mostrará a continuación busca cumplir con todo lo pedido anteriormente.   
